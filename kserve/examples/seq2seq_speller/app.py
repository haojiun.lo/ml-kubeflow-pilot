import os
import zipfile
from typing import Dict

import boto3
import kserve

from optimum.onnxruntime import ORTModelForSeq2SeqLM
from transformers import AutoTokenizer


class SpellCorrecter:
    """
    seq2seq spell correcter
    """
    def __init__(self, model, tokenizer, input_max_length=16):
        self.model = model
        self.tokenizer = tokenizer
        self.input_max_length = input_max_length

    def __call__(self, inputs, max_new_tokens=10):
        inputs = self.tokenizer(
            inputs,
            max_length=self.input_max_length,
            truncation=True,
            padding="longest",
            return_tensors="pt",
        )
        input_ids = inputs.input_ids
        attention_mask = inputs.attention_mask
        outputs = self.model.generate(input_ids, attention_mask=attention_mask,
                                      num_beams=2, num_return_sequences=1,
                                      max_new_tokens=max_new_tokens,
                                      output_scores=True, return_dict_in_generate=True
                                      )
        output_str = self.tokenizer.batch_decode(
            outputs["sequences"], skip_special_tokens=True, clean_up_tokenization_spaces=True)
        output_score = outputs.sequences_scores.exp().cpu().tolist()
        return output_str, output_score


class SpellerModel(kserve.Model):
    def __init__(self, name: str):
        super().__init__(name)
        self.name = name
        self.load()

    def load(self):
        # Download model from s3.
        model_store_bucket = os.environ.get("S3_STORE_BUCKET", None)
        model_store_key = os.environ.get("S3_STORE_KEY", None)

        s3_client = boto3.client("s3")
        s3_client.download_file(
            model_store_bucket, model_store_key, "LATEST.zip")
        with zipfile.ZipFile("LATEST.zip", 'r') as zip_ref:
            zip_ref.extractall("LATEST")
        local_path = "LATEST"

        # Load and init model
        model = ORTModelForSeq2SeqLM.from_pretrained(local_path)
        tokenizer = AutoTokenizer.from_pretrained(local_path)

        self.model = SpellCorrecter(model, tokenizer)

    def predict(self, request: Dict) -> Dict:
        query = request.get("instances", None)
        if query is None:
            return {"error": "Key 'instances' not found in request."}
        result = self.model(query)
        return {"result": result}


if __name__ == "__main__":
    # model name 'speller-custom-model' should be the same as in serve.yaml name.
    speller_model = SpellerModel("speller-custom-model")
    kserve.ModelServer().start([speller_model])
