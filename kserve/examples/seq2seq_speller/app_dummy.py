from typing import Dict

import kserve


class CopyCatModel(kserve.Model):
    def __init__(self, name: str):
        super().__init__(name)
        self.name = name
        self.load()

    def load(self):
        # init model
        self.model = lambda x: "copy cat: " + x

    def predict(self, request: Dict) -> Dict:
        query = request.get("data", None)
        if query is None:
            return {"error": "Key 'data' not found in request."}
        result = self.model(query)
        return {"result": result}


if __name__ == "__main__":
    # model name 'speller-custom-model' should be the same as in serve.yaml name.
    copy_cat_model = CopyCatModel("copy-cat-model")
    kserve.ModelServer(
        http_port=8080,
        grpc_port=8081
        ).start([copy_cat_model])
