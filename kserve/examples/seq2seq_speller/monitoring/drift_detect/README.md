# Text drift detection on speller inputs
We are going to set a drift detector for the speller model. This example will deploy a MMD([Maximum Mean Discrepancy (MMD) detector](http://jmlr.csail.mit.edu/papers/v13/gretton12a.html)) to detect input data drift.
Though it's not very straightforward when dealing with text compare to image or tabular data. Hence, we extract embeddings for the text and detect drift on those.
The alibi-detect library contains functionality to leverage pre-trained embeddings from [HuggingFace’s transformer package](https://github.com/huggingface/transformers) but also allows you to easily use your own embeddings of choice.

## Build MMD detector
First follow the notebook [here](text-drift-detect-mmd.ipynb) to build the drift detecor using your data and maybe tweak the parameter a little bit. In the end, you need to save your detector and upload to a storage like s3 for deployment. The example detector is in wtg's s3 here `s3://orca-lab-bucket/kubeflow-pilot/seq2seq_speller/alibi_detect/mmd/`

## Setup alibi drift detector server
Here we configure `seldonio/alibi-detect-server` to create the Drift Detector.
- You need to specify the detector URI using `--storage_uri` and set `serviceAccountName: default-editor` to access s3.
- Also, forward replies to the message-dumper using `--reply_url`.
- Set the input protocol that your model service is using. Check the supported protocal [here](https://github.com/SeldonIO/seldon-core/blob/5591c42b6a40a44641b848d86f9228f623c64598/components/alibi-detect-server/adserver/protocols/__init__.py).
- Notice the `drift_batch_size`. The drift detector will wait until drift_batch_size number of requests are received before making a drift prediction. And you need to add annotations `sidecar.istio.io/inject: "false"` here as well.
> **Noted**: `seldonio/alibi-detect-server` [docker image](https://github.com/SeldonIO/seldon-core/tree/5591c42b6a40a44641b848d86f9228f623c64598/components/alibi-detect-server) still use `kfserving` for data downloading and it failed to download files from s3, so maybe you should modified the source code to use `kserve` instead of out dated `kfserving` and build your own `alibi-detect-server` docker image. You can follow the instruction [here](alibi-detect-server.md) to build the docker image.

> **Noted**: Make sure the version of `alibi-detect` package you're using to build the detector is the same as the `alibi-detect-server` docker image is using, otherwise the detector may not be loaded properly.

* yaml file:
```yaml
apiVersion: serving.knative.dev/v1
kind: Service
metadata:
  name: speller-drift-detector
  namespace: kubeflow-profile
spec:
  template:
    metadata:
      annotations:
        autoscaling.knative.dev/minScale: "1"
        sidecar.istio.io/inject: "false"
    spec:
      serviceAccountName: default-editor
      containers:
      - image: haojiun/alibi-detect-server:latest
        imagePullPolicy: IfNotPresent
        args:
        - --model_name
        - speller_cd
        - --http_port
        - '8080'
        - --protocol
        - tensorflow.http
        - --storage_uri
        - s3://orca-lab-bucket/kubeflow-pilot/seq2seq_speller/alibi_detect/mmd/
        - --reply_url
        - http://speller-event-display.kubeflow-profile
        - --event_type
        - org.kubeflow.serving.inference.outlier
        - --event_source
        - org.kubeflow.serving.speller_cd
        - DriftDetector
        - --drift_batch_size
        - '10'
        resources:
            limits:
              memory: 6Gi
            requests:
              memory: 4Gi
```

# Setup kserve model service
Create the Kserve speller model.

* The annotations `sidecar.istio.io/inject: "false"` also need to be added.
* specify `serviceAccountName: default-editor` to access s3
* To assign broker, add logger in the spec.predictor. The logger.url is the broker url.

> **Noted**: The model service need to follow the protocol that `alibi-detect-server` support otherwise it can't receive requests. You can check the supported protocal [here](https://github.com/SeldonIO/seldon-core/blob/5591c42b6a40a44641b848d86f9228f623c64598/components/alibi-detect-server/adserver/protocols/__init__.py).

```yaml
apiVersion: serving.kserve.io/v1beta1
kind: InferenceService
metadata:
  name: speller-custom-model
  namespace: kubeflow-profile
  annotations:
    sidecar.istio.io/inject: "false"
spec:
  predictor:
    # minReplicas: 0  # enable scale to zero
    serviceAccountName: default-editor
    containers:
      - name: kserve-container
        image: haojiun/speller-kserve-test:v1
        env:
          - name: S3_STORE_BUCKET
            value: orca-lab-bucket
          - name: S3_STORE_KEY
            value: blues/seq2seq_model/LATEST.zip
        resources:
          limits:
            cpu: 2000m
            memory: 6Gi
          requests:
            cpu: 1000m
            memory: 4Gi
    logger:
        mode: all
        url: http://broker-ingress.knative-eventing.svc.cluster.local/kubeflow-profile/default
```

## Setup broker
* yaml file:
```yaml
apiVersion: eventing.knative.dev/v1
kind: broker
metadata:
  name: default
  namespace: kubeflow-profile
```

## Setup message-dumper
Here we use a prebuilt docker image `gcr.io/knative-releases/knative.dev/eventing-contrib/cmd/event_display` provided by knative, [details here](https://github.com/knative/eventing). Or you can build your own endpoint described in `monitoring/examples/custom_monitor`. And as mentioned in `monitoring/examples/custom_monitor`, you need to add annotations `sidecar.istio.io/inject: "false"`

* yaml file:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: speller-event-display
  namespace: kubeflow-profile
spec:
  replicas: 1
  selector:
    matchLabels: &labels
      app: speller-event-display
  template:
    metadata:
      labels: *labels
      annotations:
        sidecar.istio.io/inject: "false"
    spec:
      containers:
        - name: event-display
          image: gcr.io/knative-releases/knative.dev/eventing-contrib/cmd/event_display
---
kind: Service
apiVersion: v1
metadata:
  name: speller-event-display
  namespace: kubeflow-profile
spec:
  selector:
    app: speller-event-display
  ports:
  - protocol: TCP
    port: 80
    targetPort: 8080
```

## Setup trigger
```yaml
apiVersion: eventing.knative.dev/v1
kind: Trigger
metadata:
  name: speller-drift-trigger
  namespace: kubeflow-profile
spec:
  broker: default
  filter:
    attributes:
      type: org.kubeflow.serving.inference.request
  subscriber:
    ref:
      apiVersion: serving.knative.dev/v1
      kind: Service
      name: speller-drift-detector
      namespace: kubeflow-profile
```
Trigger connects broker and subscriber which are default broker and `speller-drift-detector` we setup before. Noted that we are detecting input data drifting so `type` is set to `org.kubeflow.serving.inference.request`.

## Testing
After applying all the yaml file above, we can test if the drift detector is working properly. You can follow the notebook [here](text-drift-detect-mmd-testing.ipynb) to send request to the model service and check the logs in `speller-drift-detector` pod like this:
```bash
kubectl logs <drift-detector-pod> -n <namespace>
```
Because we set `drift_batch_size` to 10. Hence if the number of requests less than 10 you can see something like this
```
[[I 220913 11:02:43 cd_model:79] ----
[I 220913 11:02:43 cd_model:124] Not running drift detection. Batch size is 1. Need 10
[I 220913 11:02:43 web:2275] 200 POST / (127.0.0.1) 1.48ms
```
and nothing will show in the `speller-event-display` yet. Once the requests number reach `--drift_batch_size`. The drift detector will run and you can check the result of `drift-detctor` in `speller-event-display`'s log like this:
```
kubectl --namespace kubeflow-profile logs -f -l app=speller-event-display
```
You should see the event like this:
```
Extensions,
  component: predictor
  datacontenttype: application/json
  endpoint:
  inferenceservicename: speller-custom-model
  knativearrivaltime: 2022-09-13T11:11:25.464074925Z
  namespace: kubeflow-profile
  traceparent: 00-278556219d2a017a619fe437f29bc386-85dcd855168f7eba-00
Data,
  {
    "data": {
      "is_drift": 1,
      "distance": 0.7284696698188782,
      "p_val": 0.0,
      "threshold": 0.05,
      "distance_threshold": 0.02581942081451416
    },
    "meta": {
      "name": "MMDDriftTF",
      "detector_type": "offline",
      "data_type": null,
      "version": "0.9.0",
      "backend": "tensorflow"
    }
  }
```
`is_drift` is `1` means it detects the data drifting.
