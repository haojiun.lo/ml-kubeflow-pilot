# Alibi Detect Server
* clone seldon-core repo and enter the alibi-detect dir
```bash
git clone https://github.com/SeldonIO/seldon-core.git
cd seldon-core/components/alibi-detect-server
```

* modified the requirements in `pyproject.toml`, replace `kfserving = "^0.3"` to `kserve = "^0.7"`
* modified the source code in `components/alibi-detect-server/adserver/base/storage.py`

change
```python
import kfserving
```
to
```python
import kserve
```
and
```python
kfserving.Storage.download(storage_uri)
```
to
```python
kserve.Storage.download(storage_uri)
```
in `download_model` function

* get local repo
```
cp ../../version.txt version.txt
cp -r ../../python/ _seldon_core/
```
* Need to install [poetry](https://python-poetry.org/docs/) first and use it to manage dependencies
```
poetry lock
```
and `poetry.lock` will be updated
* build docker image
```
docker build -f Dockerfile --build-arg BASE_IMAGE=seldonio/conda-ubi8 --build-arg PYTHON_VERSION=3.7.10 --build-arg CONDA_VERSION=4.7.12 --build-arg VERSION=1.14.1 -t ${REPO}/${IMAGE}:${VERSION} .
```
Adding `--platform linux/amd64` tag if you're using m1 mac.
