# How to serve model with kserve
## Serving a dummy custom model in local
Here shows how to serve a dummy custom model with `Kserve` in local.

```
# Install kserve
pip install kserve

# Start the server
# Under ml-kubeflow-pilot/kserve/examples/seq2seq_speller
python app_dummpy.py
```
After the dummy server running, we can post request with 'curl' command in another terminal to test.<br>
The `input.json` is an example data in `ml-kubeflow-pilot/kserve/examples/seq2seq_speller`
```
curl localhost:8080/v1/models/copy-cat-model:predict -d @./input.json
```
You should see the result:
```
{"result": "copy cat: appleiphone13"}
```
## Serving custom model in Kubeflow
1. Implement the custom like `app.py`.<br>
```
# Inherit from this class 'kserve.Model'
class SpellerModel(kserve.Model):
```
2. Write the yaml file like `serve.yaml`
```
apiVersion: serving.kserve.io/v1beta1
kind: InferenceService
metadata:
  name: speller-custom-model
  annotations:
    sidecar.istio.io/inject: "false"
spec:
  predictor:
    minReplicas: 0  # enable scale to zero
    serviceAccountName: default-editor
    containers:
      - name: kserve-container
        image: blueslin/speller-kserve-test:latest
        env:
          - name: S3_STORE_BUCKET
            value: orca-lab-bucket
          - name: S3_STORE_KEY
            value: blues/seq2seq_model/LATEST.zip
        resources:
          limits:
            cpu: 2000m
            memory: 6Gi
          requests:
            cpu: 1000m
            memory: 4Gi
```
3. apply with `kubectl`
```
kubectl apply -f serve.yaml
```
4. After applied, the kserve in k8s will deploy a `InferenceService` k8s object. You can check the object by this:
```
kubectl get inferenceservice
NAME                   URL                                                        READY   PREV   LATEST   PREVROLLEDOUTREVISION   LATESTREADYREVISION                            AGE
speller-custom-model   http://speller-custom-model.kubeflow-profile.example.com   True           100                              speller-custom-model-predictor-default-00004   9d
```
For more detail information:
```
kubectl describe InferenceService speller-custom-model
```
5. Test the api. There are two `URL` in the description.
```
http://speller-custom-model.kubeflow-profile.svc.cluster.local
http://speller-custom-model-predictor-default.kubeflow-profile.example.com
```
The first url with `.local` is for internal traffic usage. Means you can reach this api as long as you access inside the same k8s, for example in kubeflow notebook pod, you can test the api by the follow commnad.
```
curl http://speller-custom-model.kubeflow-profile.svc.cluster.local/v1/models/speller-custom-model:predict -d @./input.json
{"result": [["apple iphone 13"], [0.9689677357673645]]}
```
If `minReplicas` is set to 0 and there is no traffic to the serving model recently, there will be no serving pod available. But the service will automatically start scaling-up, the respond will be lated few minutes.<br>
The second url is for external traffic. You can access through istio-dex: https://github.com/kserve/kserve/tree/master/docs/samples/istio-dex
