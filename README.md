# Kubeflow Pilot Project

The purpose of this project is building a first project utilize tools in Kubeflow for automating machine learning related stuffs.

## User Guide

Check on example projects under `pipelines/examples`.<br>

### Pipelines
[pipeline overview](pipelines/README.md)


### Serving
[kserve overview](kserve/examples/seq2seq_speller/README.md)

## Dev Guide

pipelines directory structure
```
pipelines
│
├── components           # general component utils
│   └── yaml_files       # ready to use components
│
└── examples
    └── seq2seq_speller  # project related stuff
```
