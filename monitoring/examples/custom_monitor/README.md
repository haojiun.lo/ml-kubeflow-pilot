# How to monitor kserve model with knative eventing.
This example will show how to monitor both request and response in the kserve model with knative eventing.<br>
If you are not familiar with knative eventing, you can check here:
- https://knative.dev/docs/eventing/
- https://shopadmin.atlassian.net/wiki/spaces/WTG/pages/2870706410/Knative+eventing

## Monitoring examples
In this exmaple, we will setup a kserve model, `copy-cat-model`, and a simple server `event-logger` as sink for catch events from `copy-cat-model`.<br>
The event flow like this:
```
copy-cat-model -> broker -> trigger -> event-logger
```

### Setup event sink
Here is a simple example to build a sink endpoint in `sink/app.py`. The image of sink is built and push to `docker.io/blueslin/knative-event-example:sink`. We use `deployment` and `service` to deploy this sink.<br>
It is important to add annotations `sidecar.istio.io/inject: "false"` here or the event can not be corrected sent, because kserve has some issue with istio sidecar.
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: event-logger
  namespace: kubeflow-profile
spec:
  replicas: 1
  selector:
    matchLabels: &labels
      app: event-logger
  template:
    metadata:
      labels: *labels
      annotations:
        sidecar.istio.io/inject: "false"
    spec:
      containers:
        - name: event-logger
          image: docker.io/blueslin/knative-event-example:sink
          imagePullPolicy: IfNotPresent
---
# Service that exposes event-logger app.
# This will be the subscriber for the Trigger
apiVersion: v1
kind: Service
metadata:
  name: event-logger
  namespace: kubeflow-profile
spec:
  selector:
    app: event-logger
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8080
```
### Setup broker and trigger
Setup broker and trigger is more like config k8s spec.<br>
#### Broker
Broker setup yaml file:
```
apiVersion: eventing.knative.dev/v1
kind: Broker
metadata:
  name: event-logger-broker
  namespace: kubeflow-profile
```
After apply the broker, you can check with `kubectl` command:
```
% kubectl get broker -n kubeflow-profile
NAME                  URL                                                                                             AGE   READY   REASON
event-logger-broker   http://broker-ingress.knative-eventing.svc.cluster.local/kubeflow-profile/event-logger-broker   61m   True
```
The URL here `http://broker-ingress.knative-eventing.svc.cluster.local/kubeflow-profile/event-logger-broker` will need to setup in `Source` deploy later.

#### Trigger
Trigger setup yaml file:
```
apiVersion: eventing.knative.dev/v1
kind: Trigger
metadata:
  name: event-logger
  namespace: kubeflow-profile
spec:
  broker: event-logger-broker
  filter:
    attributes:
      type: org.kubeflow.serving.inference.response
  subscriber:
    ref:
      apiVersion: v1
      kind: Service
      name: event-logger
```
Trigger connects `Broker` and `Sink`, so both of them need to be specified. `Broker` can be set under `sepc.broker`. `Sink` can be set under `spec.subscriber`.<br>
The `spec.filter` here help filtering the events sent to `Sink`. The kserve event type:
- `type: org.kubeflow.serving.inference.request` the request to kserve.
- `type: org.kubeflow.serving.inference.response` the response from kserve.
- `inferenceservicename: {kserve-model-name}` send both request and response from kserve. In this example `{kserve-model-name}` would be `copy-cat-model`.

Check
[Here](https://knative.dev/docs/eventing/triggers/#trigger-annotations)
 for more trigger detail.

### Setup event source (kserve model)
We use a custom kserve model `copy-cat-model` as our source in `serving/app.py`. You can find the kserve deploy in `monitor_example.yaml`.<br>
- The annotations `sidecar.istio.io/inject: "false"` also needs to be added.
- To assign broker, add `logger` in the `spec.predictor`. The `logger.url` is the `broker` url.
```
apiVersion: serving.kserve.io/v1beta1
kind: InferenceService
metadata:
  namespace: kubeflow-profile
  name: copy-cat-model
  annotations:
    sidecar.istio.io/inject: "false"
spec:
  predictor:
    minReplicas: 1  # set 0 enable scale to zero
    serviceAccountName: default-editor
    containers:
      - name: kserve-container
        image: blueslin/knative-event-example:kserve-model
    logger:
      mode: all
      url: http://broker-ingress.knative-eventing.svc.cluster.local/kubeflow-profile/event-logger-broker
```

## Connect all
All above setting is in file `monitor_example.yaml`, you can apply it to deploy whole example by this command.
```
kubectl apply -f monitor_example.yaml
```
### Testing
To test the monitoring event, we can run a `curl` pod to fire request to `copy-cate-model`, or we can also fire the request in the kubeflow notebook.
```
kubectl --namespace kubeflow-profile run curl --image=radial/busyboxplus:curl -it
```
curl command:
```
curl http://copy-cat-model-predictor-default.kubeflow-profile.svc.cluster.local/v1/models/copy-cat-model:predict -d '{"data": "appleiphone14"}'
```
You can find the serving endpoint url by describe `inferenceservice` and check under `Status.Address.Url`.
```
kubectl describe inferenceservice copy-cat-model -n kubeflow-profile
```
Check event-logger log:
```
kubectl --namespace kubeflow-profile logs -f -l app=event-logger
```
You should see the event like this:
```
192.168.145.58 - - [02/Sep/2022 02:06:49] "POST / HTTP/1.1" 204 -
[2022-09-02 02:25:20,563] DEBUG in app: {'attributes': {'specversion': '1.0', 'id': 'b706816a-ef9d-4341-8daf-cffb6aca1a9f', 'source': 'http://localhost:9081/', 'type': 'org.kubeflow.serving.inference.response', 'datacontenttype': 'application/json; charset=UTF-8', 'time': '2022-09-02T02:25:20.539973942Z', 'component': 'predictor', 'endpoint': '', 'inferenceservicename': 'copy-cat-model', 'knativearrivaltime': '2022-09-02T02:25:20.544250615Z', 'namespace': 'kubeflow-profile', 'traceparent': '00-3177682c342d2faa3ffd1890d324c194-a8f83c1e933f2a4d-00'}, 'data': {'result': 'copy cat: appleiphone14'}}
```
