import json

from cloudevents.http import from_http
from flask import Flask, request


app = Flask(__name__)


@app.route('/', methods=['POST'])
def event_log():
    event = from_http(request.headers, request.get_data())
    app.logger.debug(event)

    return json.dumps({'success': True}), 204


@app.route('/health', methods=['GET'])
def health():
    return '', 204


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)
