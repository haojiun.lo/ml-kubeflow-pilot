# How to do drift detection in kserve model using alibi-detect
Here we will show how to use alibi-detect to do drift detection base on the [exmaple notebook](https://github.com/kserve/kserve/blob/master/docs/samples/drift-detection/alibi-detect/cifar10/cifar10_drift.ipynb) provide in [documents](https://docs.seldon.io/projects/alibi-detect/en/latest/index.html). This example we will deploy an image classification model along with a drift detector trained on the same dataset.
The architecture used is shown below and links the payload logging available within KServe with asynchronous processing of those payloads in KNative.

![arch](architecture.png)
## Setup broker
* yaml file:
```yaml
apiVersion: eventing.knative.dev/v1
kind: broker
metadata:
  name: default
  namespace: kubeflow-profile
```

## Setup message-dumper
Here we use a prebuilt docker image `gcr.io/knative-releases/knative.dev/eventing-contrib/cmd/event_display` provided by knative, [details here](https://github.com/knative/eventing). Or you can build your own endpoint described in `monitoring/examples/custom_monitor`. And as mentioned in `monitoring/examples/custom_monitor`, you need to add annotations `sidecar.istio.io/inject: "false"`

* yaml file:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-display
  namespace: kubeflow-profile
spec:
  replicas: 1
  selector:
    matchLabels: &labels
      app: hello-display
  template:
    metadata:
      labels: *labels
      annotations:
        sidecar.istio.io/inject: "false"
    spec:
      containers:
        - name: event-display
          image: gcr.io/knative-releases/knative.dev/eventing-contrib/cmd/event_display
---
kind: Service
apiVersion: v1
metadata:
  name: hello-display
  namespace: kubeflow-profile
spec:
  selector:
    app: hello-display
  ports:
  - protocol: TCP
    port: 80
    targetPort: 8080
```

## Setup kserve model service
Create the Kserve image classification model for Cifar10.

* The annotations `sidecar.istio.io/inject: "false"` also need to be added.
* To assign broker, add logger in the `spec.predictor`. The logger.url is the broker url.

```yaml
apiVersion: "serving.kserve.io/v1beta1"
kind: "InferenceService"
metadata:
  name: "tfserving-cifar10"
  namespace: kubeflow-profile
  annotations:
    sidecar.istio.io/inject: "false"
spec:
  predictor:
    tensorflow:
      storageUri: "gs://seldon-models/tfserving/cifar10/resnet32"
    logger:
      mode: all
      url: http://broker-ingress.knative-eventing.svc.cluster.local/kubeflow-profile/default
```

## Setup alibi drift detector
Here we configure `seldonio/alibi-detect-server` to create the pretrained Drift Detector. We forward replies to the message-dumper we started using `--reply_url`. Notice the `drift_batch_size`. The drift detector will wait until `drift_batch_size` number of requests are received before making a drift prediction. And you need to add annotations `sidecar.istio.io/inject: "false"` here as well.

* yaml file:
```yaml
apiVersion: serving.knative.dev/v1
kind: Service
metadata:
  name: drift-detector
  namespace: kubeflow-profile
spec:
  template:
    metadata:
      annotations:
        autoscaling.knative.dev/minScale: "1"
        sidecar.istio.io/inject: "false"
    spec:
      containers:
      - image: seldonio/alibi-detect-server:0.0.2
        imagePullPolicy: IfNotPresent
        args:
        - --model_name
        - cifar10cd
        - --http_port
        - '8080'
        - --protocol
        - tensorflow.http
        - --storage_uri
        - gs://seldon-models/alibi-detect/cd/ks/cifar10
        - --reply_url
        - http://hello-display.kubeflow-profile
        - --event_type
        - org.kubeflow.serving.inference.outlier
        - --event_source
        - org.kubeflow.serving.cifar10cd
        - DriftDetector
        - --drift_batch_size
        - '5000'
```

## Setup trigger
```yaml
apiVersion: eventing.knative.dev/v1
kind: Trigger
metadata:
  name: drift-trigger
  namespace: kubeflow-profile
spec:
  broker: default
  filter:
    attributes:
      type: org.kubeflow.serving.inference.request
  subscriber:
    ref:
      apiVersion: serving.knative.dev/v1
      kind: Service
      name: drift-detector
      namespace: kubeflow-profile
```
Trigger connects broker and subscriber which are default broker and drift-detector we setup before. Noted that we are detecting input data drifting so `type` is set to `org.kubeflow.serving.inference.request`.

## Testing
After applying all the yaml file above, we can test if the drift detector is working properly. You use the code in `cifar10-drift-detect-test.ipynb` to request the model service and check the logs in `drift-detector` pod like this:
```bash
kubectl logs <drift-detector-pod> -n <namespace>
```
Because we set `drift_batch_size` to 5000. Hence if the number of requests less than 5000 you can see something like this
```
[I 220902 10:11:44 cd_model:106] ----
[I 220902 10:11:44 cd_model:139] Not running drift detection. Batch size is 1. Need 5000
[I 220902 10:11:44 web:2243] 200 POST / (127.0.0.1) 5848.28ms
```
and nothing will show in the event-display yet. Once the requests number reach 5000. The drift detector will run and you can check the result of `drift-detctor` in event-display's log like this:
```
kubectl --namespace kubeflow-profile logs -f -l app=hello-display
```
You should see the event like this:
```
Extensions,
  component: predictor
  datacontenttype: application/json
  endpoint:
  inferenceservicename: tfserving-cifar10
  knativearrivaltime: 2022-09-02T07:00:31.143611289Z
  namespace: kubeflow-profile
  traceparent: 00-0c5b838875e054dc909c513d85972919-6c3aa88cffe80768-00
Data,
  "{\"data\": {\"batch_score\": null, \"feature_score\": null, \"is_drift\": 1, \"p_val\":
```
`is_drift` is `1` means it detects the data drifting.

Details can be found in `cifar10-drift-detect-test.ipynb`
