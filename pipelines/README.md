# Kubeflow Pipeline
A Kubeflow pipeline is a portable and scalable definition of an ML workflow, based on containers. This page is to introduce the prebuilt general components and establishes a guildeline of how you can implement your project related components in order to build your ML pipeline and run experiments in Kubeflow.
# Table of Contents
- [Components](#components)
  - [General Components](#general-components)
    - [Download version controlled data/files](#download-version-controlled-datafiles)
    - [Download from s3](#download-from-s3)
    - [Display metrics](#display-metrics)
    - [Upload to s3](#upload-to-s3)
    - [Upload kubeflow experiment run's parameters](#upload-kubeflow-experiment-runs-parameters)
  - [Project related components](#project-related-components)
    - [Building Python function-based components](#building-python-function-based-components)
    - [Building Components by creating a containerized application](#building-components-by-creating-a-containerized-application)
- [Pipelines](#pipelines)
  - [Build a pipeline using components](#build-a-pipeline-using-components)
    - [Component factory function](#component-factory-function)
    - [Pipeline function](#pipeline-function)
    - [Compile and run your pipeline](#compile-and-run-your-pipeline)

# Components
## General Components
All the general components are in `.yaml` format saved in `components/yaml_files/`. And the corresponding script that create those yaml file are at `components/*_utils`
### Download version controlled data/files
Component that take `git repo`, `git hash` and `data_path` as input and download the files version controlled by the specified DVC repo and output the downloaded file's Path.
* yaml file: `components/yaml_files/download_dvc_data_from_s3.yaml`

### Download from s3
Input `bucket_name` and `s3_key` the component will download it from s3 and output its' path.
* yaml file: `components/yaml_files/download_s3.yaml`

### Display metrics
Take the path of the `metrics.json` as input and export the metrics dictionary for Kubeflow Pipelines UI to render. So you need to generate the `metrics.json` with this format before using this component,
```
metrics = {
    'metrics': [{
      'name': 'accuracy-score', # The name of the metric. Visualized as the column name in the runs table.
      'numberValue':  accuracy, # The value of the metric. Must be a numeric value.
      'format': "PERCENTAGE",   # The optional format of the metric. Supported values are "RAW" (displayed in raw format) and "PERCENTAGE" (displayed in percentage format).
    }]
  }
```
* yaml file: `components/yaml_files/display_metrics.yaml`

### Upload to s3
This component takes `bucket_name`, `s3_key` and `exp_working_dir` as input and upload the contents inside `exp_working_dir` to s3 under `bucket_name` and `s3_key`
* yaml file: `components/yaml_files/upload_to_s3.yaml`

### Upload kubeflow experiment run's parameters
Upload experiment run's meta data including parameters to s3  under specufied `bucket_name` and `s3_key`.
* yaml file: `components/yaml_files/upload_exp_params.yaml`

## Project related components
Kubeflow pipeline components are containerized applications that perform a step in your ML workflow. Here are the ways that you can define pipeline components:

### Building Python function-based components
If your component code can be expressed as a Python function. You can easily use Kubeflow Pipelines SDK to build lightweight Python function-based components. Details can be found [here](https://www.kubeflow.org/docs/components/pipelines/sdk/python-function-components/).

### Building Components by creating a containerized application
If your component code is hard to be expressed as single Python function you can create a containerized application as a pipeline component. When Kubeflow Pipelines executes a component, a container image is started in a Kubernetes Pod and your component’s inputs are passed in as command-line arguments. When your component has finished, the component’s outputs are returned as files.

#### step 1 Designing the component’s code
Component’s code can be a python script like the following example
[seq2seq speller training script](examples/seq2seq_speller/src/run_experiment.py)
[model export script](examples/seq2seq_speller/src/export_to_onnx.py)

* When you define your component, you need to let Kubeflow Pipelines know what outputs your component produces. So when your pipeline runs, Kubeflow Pipelines passes the paths that you use to store your component’s outputs as inputs to your component.
* It is necessary to create parent directories for your output file or dir if they do not exist.
* Your component’s code can be implemented in any language, so long as it can run in a container image.


#### step 2 Containerize your component’s code
In this step, you need to create a docker container image for your component's code to run in. You can refer to the `seq2seq exmaple`'s [dockerfile here](examples/seq2seq_speller/src/dockerfile). Once your dockerfile is ready, you need to build it and push it to a container registry that your K8s cluster can access. You can write a script to do this like [the example](examples/seq2seq_speller/src/build_image.sh).


#### step 3 Creating a component specification
Last step is to create component specification. A component specification contains component’s implementation, interface, and metadata.
##### component’s implementation
1. create a yaml file with your component name
2. Create your component’s implementation section and specify the name of your container image
3. Define a `command` for your component’s implementation. This field specifies the command-line arguments that are used to run your program in the container.
Following is the `seq2seq trainer example`
```yaml
implementation:
  container:
    # The strict name of a container image that you've pushed to a container registry.
    image: haojiun/speller-kf-pilot:latest
    # command is a list of strings (command-line arguments).
    # The YAML language has two syntaxes for lists and you can use either of them.
    # Here we use the "flow syntax" - comma-separated strings inside square brackets.
    command: [
      python3,
      # Path of the program inside the container
      run_experiment.py,
      --model_name_or_path,
      {inputValue: model_name_or_path},
      --data_path,
      {inputPath: data_path},
      --dynamic_noise,
      {inputValue: dynamic_noise},
      --max_steps,
      {inputValue: max_steps},
      --batch_size,
      {inputValue: batch_size},
      --learning_rate,
      {inputValue: learning_rate},
      --gradient_accumulation_steps,
      {inputValue: gradient_accumulation_steps},
      --output_path,
      {outputPath: output_path}
    ]
```
There are three types of input/output placeholders:
* {inputValue: <input-name>}: This placeholder is replaced with the value of the specified input. This is useful for small pieces of input data, such as numbers or small strings.

* {inputPath: <input-name>}: This placeholder is replaced with the path to this input as a file. Your component can read the contents of that input at that path during the pipeline run.

* {outputPath: <output-name>}: This placeholder is replaced with the path where your program writes this output’s data. This lets the Kubeflow Pipelines system read the contents of the file and store it as the value of the specified output.

The <input-name>/<output-name> name must match the name of an input/output in the inputs/outputs section of your component specification.

##### Define your component’s interface
You need to define the inputs and outputs in your yaml file. `seq2seq trainer example` is as follows:
```yaml
inputs:
    - {name: model_name_or_path, type: String, description: 'pretrained model name or path', default: 'facebook/bart-base'}
    - {name: data_path, type: String, description: 'dir contains train, val, test files'}
    - {name: dynamic_noise, type: Bool, description: 'whether to dynamically generate typo during training', default: 'True'}
    - {name: max_steps, type: Integer, description: 'the total number of training steps to perform', default: '10'}
    - {name: batch_size, type: Integer, description: 'num of batch size', default: '128'}
    - {name: learning_rate, type: Float, description: 'The initial learning rate for [`AdamW`] optimizer.', default: '2e-4'}
    - {name: gradient_accumulation_steps, type: Integer, description: 'Number of updates steps to accumulate the gradients for, before performing a backward/update pass.', default: 1}
    - {name: fp16, type: Bool, description: 'Whether to use fp16 16-bit (mixed) precision training', default: 'True'}

outputs:
    - {name: output_path, type: String}
```
The `Inputs` item has following attributes:
* name: Human-readable name of this input. Each input’s name must be unique.
* description: (Optional.) Human-readable description of the input.
* default: (Optional.) Specifies the default value for this input.
* type: (Optional.) Specifies the input’s type. Learn more about the [types defined in the Kubeflow Pipelines SDK](https://github.com/kubeflow/pipelines/blob/sdk/release-1.8/sdk/python/kfp/dsl/types.py) and how [type checking works in pipelines and components](https://www.kubeflow.org/docs/components/pipelines/sdk/static-type-checking/).
* optional: Specifies if this input is optional. The value of this attribute is of type Bool, and defaults to False.

The `Outputs` item has following attributes
* name: Human-readable name of this output. Each output’s name must be unique.
* description: (Optional.) Human-readable description of the output.
* type: (Optional.) Specifies the output’s type.


##### Specify your component’s metadata
Last is to add the `name `and `description` fields to your yaml file
`seq2seq trainer example`
```yaml
name: seq2seq speller train/eval job
description: seq2seq speller train/eval job
```

`trainer` and `exporter`'s complete yaml file can be found in `examples/seq2seq_speller/yaml`

# Pipelines
## Build a pipeline using components
### Component factory function
Once you have all the components you need and store them in yaml format. You can start building your pipeline. We are going to use Kubeflow Pipelines SDK's `kfp.components.load_component_from_file` function to load the component yaml. This function create a factory function that you can use to create [ContainerOp](https://kubeflow-pipelines.readthedocs.io/en/stable/source/kfp.dsl.html#kfp.dsl.ContainerOp) instances to use as steps in your pipeline. Take `seq2seq speller` as example
First, you load all the components yaml you need using `load_component_from_file` to return factory functions that you can use to create pipeline steps:

```python
import os
import kfp
import kfp.dsl as dsl

from kfp import components

download_s3_op = components.load_component_from_file("components/yaml_files/download_dvc_data_from_s3.yaml")

trainer_op = components.load_component_from_file("examples/seq2seq_speller/yaml/trainer.yaml")

display_metrics_op = components.load_component_from_file("components/yaml_files/display_metrics.yaml")

exporter_op = components.load_component_from_file("examples/seq2seq_speller/yaml/exporter.yaml")

upload_op = components.load_component_from_file("components/yaml_files/upload_to_s3.yaml")
```

### Pipeline function
Now you can define the pipeline function. Pipeline function’s arguments define your pipeline’s parameters. Use pipeline parameters to experiment with different hyperparameters such as learning rate.
Use those factory functions you created last step to create pipeline steps([ContainerOp](https://kubeflow-pipelines.readthedocs.io/en/stable/source/kfp.dsl.html#kfp.dsl.ContainerOp)) in the pipeline function. The inputs to the factory functions can be pipeline parameters, the outputs of other tasks, or a constant value.
And you can specify custom resource to every [ContainerOp](https://kubeflow-pipelines.readthedocs.io/en/stable/source/kfp.dsl.html#kfp.dsl.ContainerOp) like the following example.

```python
@dsl.pipeline(
    name="orca-speller-pipeline",
    description="pipeline for pre-training/finetune seq2seq speller",
)
def speller_pipeline(...):
    # 1.download data from s3
    data_download = download_s3_op(...)

    # 2.model training job
    train_task = (
        trainer_op(...)
        .set_gpu_limit(1)
        .set_memory_request('16G')
        .set_cpu_request("1000m")
        .set_cpu_limit("2500m")
    )

    # 3.display testing metrics
    _ = display_metrics_op(train_task.outputs["output_path"]).after(train_task)

    # 4.model exporting
    export_task = (
        exporter_op(save_path=train_task.outputs["output_path"])
        .set_memory_request('4G')
        .set_memory_limit('8G')
    ).after(train_task)

    # 5.upload to s3
    # upload training ckpt, logs, params to s3
    _ = upload_op(..., train_task.outputs["output_path"]).after(train_task)

    # upload exported model to s3
    _ = upload_op(..., export_task.outputs["export_path"]).after(export_task)
```

### Compile and run your pipeline
Option 1: Compile and then upload in UI
1. run the code to compile the pipeline and save it as `pipeline.yaml`
```python
kfp.compiler.Compiler().compile(
        pipeline_func=speller_pipeline, package_path="pipeline.yaml"
    )
```
2. Upload and run your `pipeline.yaml` using the Kubeflow Pipelines user interface

Option 2: run the pipeline using Kubeflow Pipelines SDK client
1. Follow the steps in [connecting to Kubeflow Pipelines using the SDK client](https://www.kubeflow.org/docs/components/pipelines/sdk/connect-api)
```python
client = kfp.Client() # change arguments accordingly
```
2. You can use the `kfp.Client` instance to upload pipeline, pipeline version or create runs. Details can be found in [here](https://kubeflow-pipelines.readthedocs.io/en/latest/source/kfp.client.html#kfp.Client)
