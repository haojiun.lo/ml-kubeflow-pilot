import os

from kfp import components


def create_component_yaml(dir_path):
    file_path = os.path.join(dir_path, "download_s3.yaml")
    components.create_component_from_func(
        download_s3,
        output_component_file=file_path,
        packages_to_install=["boto3"],
        base_image="python:3.9"
    )


def download_s3(
    bucket_name: str,
    s3_key: str,
    output_dir: components.OutputPath()
):
    """Download any file or directory from s3.

    Args:
        bucket_name: s3 bucket name.
        s3_key: s3 directory path.
        output_dir: component output path.
    """
    import os
    import logging
    import boto3

    logging.basicConfig(
        format="%(levelname)s:%(asctime)s %(filename)s:%(lineno)d %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        level=logging.INFO
    )

    def download_s3_folder(bucket_name, s3_folder, local_dir=None, logger=None):
        """
        Ref: https://stackoverflow.com/questions/49772151/download-a-folder-from-s3-using-boto3
        Download the contents of a folder directory
        Args:
            bucket_name: the name of the s3 bucket.
            s3_folder: the folder path in the s3 bucket.
            local_dir: a relative or absolute directory path in the local file system.
        """
        s3 = boto3.resource('s3')
        bucket = s3.Bucket(bucket_name)
        for obj in bucket.objects.filter(Prefix=s3_folder):
            if local_dir is None:
                target = obj.key
            else:
                target_path = os.path.relpath(obj.key, s3_folder)
                if target_path.startswith(".."):
                    continue
                elif target_path == ".":
                    target = os.path.join(local_dir, obj.key.split("/")[-1])
                else:
                    target = os.path.join(local_dir, target_path)

            if not os.path.exists(os.path.dirname(target)):
                os.makedirs(os.path.dirname(target))
            if obj.key[-1] == '/':
                continue
            if logger:
                logger.info(f"Downloading {obj.key} to {target}")

            bucket.download_file(obj.key, target)

    download_s3_folder(bucket_name, s3_key, output_dir, logger=logging)


if __name__ == "__main__":
    file_dir = os.path.dirname(__file__)
    dir_path = os.path.join(file_dir, "../yaml_files")
    create_component_yaml(dir_path)
