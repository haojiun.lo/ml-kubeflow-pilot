import os

from kfp import components


def create_component_yaml(dir_path):
    file_path = os.path.join(dir_path, "upload_to_s3.yaml")
    components.create_component_from_func(
        upload_to_s3,
        output_component_file=file_path,
        packages_to_install=["boto3"],
        base_image="python:3.9"
    )


def upload_to_s3(
    bucket_name: str,
    s3_key: str,
    exp_working_dir: components.InputPath()
):
    """Upload file or content in directory to target s3 path.

    Args:
        bucket_name: s3 bucket name.
        s3_key: upload to s3 directory key.
        exp_working_dir: output path from other componetent. Contents inside will be uploaded
            to s3 under 's3_key'.
    """
    import os
    import logging
    import boto3

    logging.basicConfig(
        format="%(levelname)s:%(asctime)s %(filename)s:%(lineno)d %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        level=logging.INFO
    )

    def upload_s3(upload_path, target_s3_bucket, target_s3_prefix, logger=None):
        """Upload file or content in directory to target s3 path.

        Args:
            upload_path: path to file or directorys in local file system.
            target_s3_bucket: s3 bucket name.
            target_s3_prefix: s3 key prefix.
        """
        client = boto3.client('s3')
        if os.path.isdir(upload_path):
            if logger:
                logger.info(f"Upload contents under: {upload_path}")
            for root_path, dir_pathes, files in os.walk(upload_path):
                for file in files:
                    file_local_path = os.path.join(root_path, file)
                    target_rel_path = os.path.relpath(file_local_path, upload_path)
                    target_s3_key = os.path.join(target_s3_prefix, target_rel_path)
                    client.upload_file(file_local_path, target_s3_bucket, target_s3_key)
                    if logger:
                        logger.info(
                            f"Uploaded file {file_local_path} "
                            f"to s3://{target_s3_bucket}/{target_s3_key}"
                        )

        else:
            filename = os.path.split(upload_path)[-1]
            target_s3_key = os.path.join(target_s3_prefix, filename)
            client.upload_file(upload_path, target_s3_bucket, target_s3_key)
            if logger:
                logger.info(
                    f"Uploaded file {upload_path} "
                    f"to s3://{target_s3_bucket}/{target_s3_key}"
                )

    upload_s3(exp_working_dir, bucket_name, s3_key, logger=logging)


if __name__ == "__main__":
    file_dir = os.path.dirname(__file__)
    dir_path = os.path.join(file_dir, "../yaml_files")
    create_component_yaml(dir_path)
