import os

from kfp import components


def create_component_yaml(dir_path):
    file_path = os.path.join(dir_path, "upload_exp_params.yaml")
    components.create_component_from_func(
        upload_exp_params,
        output_component_file=file_path,
        packages_to_install=["boto3"],
        base_image="python:3.9"
    )


def upload_exp_params(
    bucket_name: str,
    s3_key: str
):
    """Upload experiment meta data include pipeline parameters to s3.

    Args:
        bucket_name: s3 bucket name.
        s3_key: upload to s3 directory key.
    """
    import os
    import logging
    import json
    import boto3

    logging.basicConfig(
        format="%(levelname)s:%(asctime)s %(filename)s:%(lineno)d %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        level=logging.INFO
    )

    workflow_name = '{{workflow.name}}'
    run_id = '{{workflow.uid}}'
    parameters = '{{workflow.parameters}}'
    params = json.loads(parameters)
    params.append({"name": "run_id", "value": run_id})
    params.append({"name": "workflow_name", "value": workflow_name})

    file_name = f"run-params-{run_id}.json"
    s3_path = os.path.join(s3_key, file_name)

    binary_data = json.dumps(params).encode()
    logging.info(f"Meta data: {params}")
    logging.info(f"Upload Meta data to: s3://{bucket_name}/{s3_path}")

    s3 = boto3.resource('s3')
    bucket_obj = s3.Bucket(bucket_name)
    bucket_obj.put_object(Key=s3_path, Body=binary_data)


if __name__ == "__main__":
    file_dir = os.path.dirname(__file__)
    dir_path = os.path.join(file_dir, "../yaml_files")
    create_component_yaml(dir_path)
