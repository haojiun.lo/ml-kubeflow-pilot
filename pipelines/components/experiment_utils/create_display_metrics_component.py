import os

from kfp.components import InputPath, OutputPath, create_component_from_func


def display_metrics(exp_working_dir: InputPath(str), mlpipeline_metrics_path: OutputPath("Metrics")):  # noqa: F821
    """Export the metrics dictionary for Kubeflow Pipelines UI to render.

    Args:
        exp_working_dir: output path from other componetent which contains standard metrics.json.
        s3_key: upload to s3 directory key.
        exp_working_dir:
    """
    import json
    import os

    with open(os.path.join(exp_working_dir, "metrics.json")) as f:
        metrics = json.load(f)

    with open(mlpipeline_metrics_path, "w") as f:
        json.dump(metrics, f)


if __name__ == "__main__":
    file_dir = os.path.dirname(__file__)
    yaml_file = os.path.join(file_dir, "../yaml_files", "display_metrics.yaml")
    create_component_from_func(
        display_metrics, output_component_file=yaml_file)
